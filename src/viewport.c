#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdbool.h>
#include <stdio.h>

#include "viewport.h"
#include "draw.h"
#include "maths.h"

#define DEBUG_FCTS


Viewport* new_viewport (int width, int height, Minesweeper* minesweeper) {
    if (SDL_Init(SDL_INIT_VIDEO)) {
        SDL_Log("Error SDL_Init - %s", SDL_GetError());
        close_viewport(NULL);
        return NULL;
    }

    Viewport* viewport = malloc(sizeof(Viewport));
    if(viewport) {
        viewport->width = width;
        viewport->height = height;
        viewport->minesweeper = minesweeper;

        viewport->vms.size_cell = 0,
        viewport->vms.x_max = 0;
        viewport->vms.x_min = 0;
        viewport->vms.x_mouse = 0;
        viewport->vms.y_min = 0;
        viewport->vms.y_max = 0;
        viewport->vms.y_mouse = 0;

        viewport->state = GAME; //Initialized at GAME while there is not menu
    }
    else close_viewport(NULL);

    SDL_Window* window = SDL_CreateWindow("CMinesweeper",
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        width, height,
        SDL_WINDOW_RESIZABLE
    );
    if (window) {
        viewport->window = window;
    }
    else {
        SDL_Log("Error SDL_Window - %s", SDL_GetError());
        close_viewport(viewport);
        viewport = NULL;
    }

    SDL_Renderer* renderer = SDL_CreateRenderer(window,
        -1,
        SDL_RENDERER_ACCELERATED
    );
    if (renderer) {
        viewport->renderer = renderer;
    }
    else {
        SDL_Log("Error SDL_Renderer - %s", SDL_GetError());
        close_viewport(viewport);
        viewport = NULL;
    }

    SDL_Texture* texture = IMG_LoadTexture(viewport->renderer, "./img/minesweeper.png");
    if (texture) {
        viewport->texture = texture;
    }
    else {
        SDL_Log("Error SDL_Texture - %s", SDL_GetError());
        close_viewport(viewport);
        viewport = NULL;
    }
    return viewport;
}

void close_viewport(Viewport* viewport) {
    if(viewport) {
        if(viewport->texture) SDL_DestroyTexture(viewport->texture);
        if(viewport->renderer) SDL_DestroyRenderer(viewport->renderer);
        if(viewport->window) SDL_DestroyWindow(viewport->window);
        free(viewport);
    }
    IMG_Quit();
    SDL_Quit();
}

void update_vms(Viewport* viewport, bool ms, bool mouse) {
    if (ms) {
        viewport->vms.x_min = 0;
        viewport->vms.x_max = viewport->width;
        viewport->vms.y_min = 0;
        viewport->vms.y_max = viewport->height;
    
        int dx = viewport->vms.x_max - viewport->vms.x_min;
        int dy = viewport->vms.y_max - viewport->vms.y_min;
        viewport->vms.size_cell = min_int(dx/viewport->minesweeper->columns, dy/viewport->minesweeper->lines);

        viewport->vms.border_x = (viewport->vms.x_max - viewport->vms.x_min - viewport->minesweeper->columns*viewport->vms.size_cell)/2;
        viewport->vms.border_y = (viewport->vms.y_max - viewport->vms.y_min - viewport->minesweeper->lines*viewport->vms.size_cell)/2;
    }
    if (mouse) {
        viewport->vms.i_mouse = (viewport->vms.y_mouse - (viewport->vms.y_min + viewport->vms.border_y))/viewport->vms.size_cell;
        viewport->vms.j_mouse = (viewport->vms.x_mouse - (viewport->vms.x_min + viewport->vms.border_x))/viewport->vms.size_cell;
    }
}

void event_loop_game(Viewport* viewport) {
    SDL_Event event;
    update_vms(viewport, true, true);
    while (viewport->state == GAME) {
        while (SDL_PollEvent(&event)) {
            switch(event.type) {
                case SDL_QUIT:
                    viewport->state = QUIT;
                break;
                case SDL_WINDOWEVENT:
                    if (event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
                        viewport->width = event.window.data1;
                        viewport->height = event.window.data2;
                        update_vms(viewport, true, false);
                    }
                break;
                case SDL_MOUSEBUTTONDOWN:
                    if (viewport->minesweeper->state != EXPLODED && viewport->minesweeper->state != COMPLETED) {
                        SDL_GetMouseState(&(viewport->vms.x_mouse), &(viewport->vms.y_mouse));
                        update_vms(viewport, false, true);
                        if (event.button.button == SDL_BUTTON_LEFT) {
                            if (viewport->minesweeper->state == NOT_INITIALIZED) {
                                init_minesweeper(viewport->minesweeper, viewport->vms.i_mouse, viewport->vms.j_mouse);
                                viewport->minesweeper->state = INITIALIZED;
                            }
                            update_game(viewport->minesweeper, viewport->vms.i_mouse, viewport->vms.j_mouse, true);
                        }
                        else if (event.button.button == SDL_BUTTON_RIGHT) {
                            update_game(viewport->minesweeper, viewport->vms.i_mouse, viewport->vms.j_mouse, false);
                        }
                        //draw_minesweeper_txt(viewport);
                    }
                break;
                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym) {
                        #ifdef DEBUG_FCTS
                        case SDLK_a:
                                if (viewport->minesweeper->state == INITIALIZED)
                                    show_all_minesweeper(viewport->minesweeper);
                        break;
                        case SDLK_m:
                                if (viewport->minesweeper->state == INITIALIZED)
                                    show_all_mines(viewport->minesweeper);
                        break;
                        case SDLK_r:
                            reset_minesweeper(viewport->minesweeper);
                            viewport->minesweeper->state = NOT_INITIALIZED;
                        break;
                        case SDLK_l:
                            free(viewport->minesweeper);
                            viewport->minesweeper = import_minesweeper();
                            update_vms(viewport, true, true);
                        break;
                        #endif
                        case SDLK_ESCAPE:
                            viewport->state = QUIT;
                        break;
                    }
                break;
                
            }
        }
        SDL_GetMouseState(&(viewport->vms.x_mouse), &(viewport->vms.y_mouse));
        update_vms(viewport, false, true);
        draw_viewport(viewport);
        SDL_RenderPresent(viewport->renderer);
        SDL_Delay(10);
    }
}

void event_loop(Viewport* viewport) {
    while (viewport->state != QUIT) {
        switch (viewport->state) {
            case MENU:
                printf("Oui\n");
            break;
            case GAME:
                event_loop_game(viewport);
            break;
        }
    }
}