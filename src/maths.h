#ifndef __MATHS_H__
#define __MATHS_H__


/**
 * @brief Returns the min between two ints
 * 
 * @param a an integer.
 * @param b an integer.
 * @return min(a, b).
 */
int min_int(int a, int b);

/**
 * @brief Returns the max between two ints
 * 
 * @param a an integer.
 * @param b an integer.
 * @return max(a, b).
 */
int max_int(int a, int b);


#endif