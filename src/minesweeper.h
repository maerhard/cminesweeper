#ifndef __MINESWEEPER_H__
#define __MINESWEEPER_H__


#include <stdbool.h>
#include "minefile.h"


typedef enum cellState {
    HIDDEN = 0,
    SHOWN,
    FLAGGED,
    QUESTION_MARK
} CellState;

typedef struct {
    int mine;
    CellState state;
    int neighbour_mines;
} Cell;

typedef enum minesweeperState {
    NOT_INITIALIZED = 0,
    INITIALIZED,
    EXPLODED,
    COMPLETED
} MsState;

typedef struct {
    int lines;
    int columns;
    int mines;
    int flags;
    int cells_left;
    File* file;
    Cell *minesweeper;
    MsState state;
} Minesweeper;


/**
 * @brief Creates a new minesweeper.
 * 
 * @param lines the number of lines of the minesweeper.
 * @param columns the number of columns of the minesweeper.
 * @param mines the number of mines of the minesweeper.
 * @return the new minesweeper. 
 */
Minesweeper* new_minesweeper(int lines, int columns, int mines);

/**
 * @brief Returns the Cell* from coordinates
 * 
 * @param ms the minesweeper containing the cell.
 * @param i the line of the cell.
 * @param j the column of the cell.
 * @return the corresponding Cell*.
 */
Cell* cell_from_coord(Minesweeper* ms, int i, int j);

/**
 * @brief Initializes a already-existing minesweeper.
 * 
 * @param minesweeper the minesweeper.
 * @param line the line of the first safe cell.
 * @param column the column of the first safe cell.
 */
void init_minesweeper(Minesweeper* minesweeper, int line, int column);

/**
 * @brief Resets a minesweeper (the ms needs to be initialize once again)
 * 
 * @param minesweeper the minesweeper
 */
void reset_minesweeper(Minesweeper* minesweeper);

/**
 * @brief Uncovers every blank cell near the one in the file.
 * 
 * @param minesweeper the minesweeper containing the file.
 */
void uncover_cells(Minesweeper* minesweeper);

/**
 * @brief Updates a game's cell in the case of a left click. If it is a mine, change the state of the minesweeper.
 * 
 * @param minesweeper the game.
 * @param line the line of the cell.
 * @param column the column of the cell.
 * @return if the cell contains a mine.
 */
void update_game_cell_lmb(Minesweeper* minesweeper, int line, int column);

/**
 * @brief Updates a game's cell in the case of a right click.
 * 
 * @param minesweeper 
 * @param line 
 * @param column 
 */
void update_game_cell_rmb(Minesweeper* minesweeper, int line, int column);

/**
 * @brief Updates the game based on the input of the player and the cell aimed.
 * 
 * @param minesweeper the minesweeper to update.
 * @param line the line of the cell aimed.
 * @param column the column of the cell aimed.
 * @param lmb true if the player click the left mouse button, false if right.
 * @return if the game is over (clicked on a mine, or cleared every safe cell).
 */
void update_game(Minesweeper* minesweeper, int line, int column, bool lmb);

/**
 * @brief Frees a minesweeper.
 * 
 * @param minesweeper the said minesweeper.
 */
void free_minesweeper(Minesweeper* minesweeper);

Minesweeper* import_minesweeper();

void export_minesweeper(Minesweeper*);



//-----------------------------
//- DEBUGGING FONCTIONS BELOW -
//-----------------------------



/**
 * @brief Sets all the cell states on SHOWN.
 * 
 * @param minesweeper the minesweeper.
 */
void show_all_minesweeper(Minesweeper* minesweeper);

/**
 * @brief Shows all the cells containing a mine.
 * 
 * @param minesweeper the minesweeper.
 */
void show_all_mines(Minesweeper* minesweeper);

#endif