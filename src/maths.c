#include "maths.h"

int min_int(int a, int b) {
    int m = a;
    if (b<a)
        m = b;
    return m;
}

int max_int(int a, int b) {
    int M = a;
    if (b>a)
        M = b;
    return M;
}