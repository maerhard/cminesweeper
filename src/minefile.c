#include <stdlib.h>
#include "minefile.h"

File* new_file() {
    File* file = malloc(sizeof(File));
    if (file) {
        file->writing_index=0;
        file->reading_index=0;
    }
    return file;
}

void free_file(File* file) {
    if (file) {
        free(file);
    }
}

int is_empty_file(File* file) {
    int res = (file->writing_index == file->reading_index)?1:0;
    return res;
}

void insert_in_file(File* file, Position pos) {
    file->file[file->writing_index] = pos;
    file->writing_index = (file->writing_index+1)%MAX_FILE;
}

Position remove_from_file(File* file) {
    Position pos = file->file[file->reading_index];
    file->reading_index = (file->reading_index+1)%MAX_FILE;
    return pos;
}

void empty_file(File* file) {
    file->reading_index=0;
    file->writing_index=0;
}