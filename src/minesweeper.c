#include <stdlib.h>
#include <stdio.h>

#include "minefile.h"
#include "minesweeper.h"



Minesweeper* new_minesweeper(int lines, int columns, int mines) {
    Minesweeper* ms = malloc(sizeof(Minesweeper));

    if (ms) {
        ms->lines = lines;
        ms->columns = columns;
        ms->mines = (mines >= lines*columns) ? lines*columns-9 : mines;
        ms->flags = 0;
        ms->cells_left = lines*columns;
        ms->file = new_file();
        ms->minesweeper = malloc(lines*columns*sizeof(Cell));
        ms->state = NOT_INITIALIZED;

        if (!ms->minesweeper || !ms->file) {
            free_minesweeper(ms);
            ms = NULL;
        }
        else {
            for (int i=0; i<lines*columns; i++) {
                ms->minesweeper[i].mine = 0;
                ms->minesweeper[i].neighbour_mines = 0;
                ms->minesweeper[i].state = HIDDEN;
            }
        }
    }

    return ms;
}

Cell* cell_from_coord(Minesweeper* ms, int i, int j) {
    return (ms->minesweeper + i*ms->columns + j);
}

void init_minesweeper(Minesweeper* minesweeper, int line, int column) {
    int rand_line, rand_column;
    Cell* rand_cell;
    for (int set_mine=0; set_mine<minesweeper->mines; set_mine++) {
        rand_line = rand()%(minesweeper->lines);
        rand_column = rand()%(minesweeper->columns);

        while ((cell_from_coord(minesweeper, rand_line, rand_column)->mine)  // Reroll rand_cell if there is already a mine
            || ((line-1 <= rand_line && rand_line <= line+1)                 //
            && (column-1 <= rand_column && rand_column <= column+1)))        // and if it is in the safe zone where the player clicked (square of 3 by side)
        {
            rand_line = rand()%(minesweeper->lines);
            rand_column = rand()%(minesweeper->columns);
        }

        cell_from_coord(minesweeper, rand_line, rand_column)->mine = 1;

        for (int i=rand_line-1; i<=rand_line+1; i++) {
            for (int j=rand_column-1; j<rand_column+2; j++) {
                if ((i>=0 && i<minesweeper->lines) && (j>=0 && j<minesweeper->columns) && (i!=rand_line || j!=rand_column))
                    cell_from_coord(minesweeper, i, j)->neighbour_mines += 1;
            }
        }
    }
}

void reset_minesweeper(Minesweeper* minesweeper) {
    for (int i=0; i<minesweeper->lines*minesweeper->columns; i++) {
        minesweeper->minesweeper[i].mine = 0;
        minesweeper->minesweeper[i].neighbour_mines = 0;
        minesweeper->minesweeper[i].state = HIDDEN;
    }
    minesweeper->cells_left = minesweeper->lines*minesweeper->columns;
}

void uncover_cells(Minesweeper* minesweeper) {
    File* file = minesweeper->file;
    Position pos_remove, pos_insert; //pos_remove is the one from the file, and pos_insert is modified and inserted in the file.
    Cell* cell_remove;
    while (!is_empty_file(file)) {
        pos_remove = remove_from_file(file);
        cell_remove = cell_from_coord(minesweeper, pos_remove.i, pos_remove.j);
        if (cell_remove->state == HIDDEN) {
            cell_remove->state = SHOWN;
            if (cell_remove->neighbour_mines == 0) {
                for (pos_insert.i=pos_remove.i-1; pos_insert.i<=pos_remove.i+1; pos_insert.i++) {
                    for (pos_insert.j=pos_remove.j-1; pos_insert.j<=pos_remove.j+1; pos_insert.j++) {
                        if ((pos_insert.i>=0 && pos_insert.i<minesweeper->lines)
                        && (pos_insert.j>=0 && pos_insert.j<minesweeper->columns)
                        && (pos_insert.i!=pos_remove.i || pos_insert.j!=pos_remove.j)
                        && (cell_from_coord(minesweeper, pos_insert.i, pos_insert.j)->state == HIDDEN)) {
                        insert_in_file(file, pos_insert);
                        }
                    }
                }
            }
        }
    }
}

void update_game_cell_lmb(Minesweeper* minesweeper, int line, int column) {
    Cell* cell = minesweeper->minesweeper + line*minesweeper->columns + column;
    if (cell->state == HIDDEN) {
        if (cell->mine) {
            cell->state = SHOWN;
            minesweeper->state = EXPLODED;
        }
        else {
            Position pos;
            pos.i = line;
            pos.j = column;
            insert_in_file(minesweeper->file, pos);
            uncover_cells(minesweeper);
        }
    }
}

void update_game_cell_rmb(Minesweeper* minesweeper, int line, int column) {
    Cell* cell = cell_from_coord(minesweeper, line, column);
    switch (cell->state) {
        case HIDDEN:
            cell->state = FLAGGED;
            minesweeper->flags += 1;
        break;
        case FLAGGED:
            cell->state = QUESTION_MARK;
            minesweeper->flags -= 1;
        break;
        case QUESTION_MARK:
            cell->state = HIDDEN;
        break;
    }
}

void update_game(Minesweeper* minesweeper, int line, int column, bool lmb) {
    if (lmb) {
        update_game_cell_lmb(minesweeper, line, column);
        if (minesweeper->state == INITIALIZED && minesweeper->mines == minesweeper->cells_left) {
            minesweeper->state = COMPLETED;
        }
    }
    else {
        update_game_cell_rmb(minesweeper, line, column);
    }
}

void free_minesweeper(Minesweeper* minesweeper) {
    if (minesweeper) {
        free_file(minesweeper->file);
        if (minesweeper->minesweeper) {
            free(minesweeper->minesweeper);
        }
        free(minesweeper);
    }
}

Minesweeper* import_minesweeper() {
    FILE* file = fopen("save.txt", "r");
    Minesweeper* ms = NULL;
    if (file) {
        int lines, columns, mines;
        fscanf(file, "%d %d %d", &lines, &columns, &mines);
        ms = new_minesweeper(lines, columns, mines);

        for(int i=0; i<lines*columns; i++) {
            fscanf(
                file, "%d %d %d",
                &(ms->minesweeper[i].mine),
                &(ms->minesweeper[i].state),
                &(ms->minesweeper[i].neighbour_mines)
            );
            switch (ms->minesweeper[i].state) {
                case 1:
                    ms->cells_left--;
                    break;
                case 2:
                    ms->flags++;
                    break;
            }
        }
    }
    fclose(file);
    return ms;
}



//-----------------------------
//- DEBUGGING FONCTIONS BELOW -
//-----------------------------



void show_all_minesweeper(Minesweeper* minesweeper) {
    for (int i=0; i<minesweeper->lines; i++) {
        for (int j=0; j<minesweeper->columns; j++) {
            minesweeper->minesweeper[i*minesweeper->columns + j].state = SHOWN;
        }
    }
}

void show_all_mines(Minesweeper* minesweeper) {
    Cell* cell = minesweeper->minesweeper;
    for (int i=0; i<minesweeper->lines; i++) {
        for (int j=0; j<minesweeper->columns; j++) {
            if ((cell + i*minesweeper->columns + j)->mine == 1)
                (cell + i*minesweeper->columns + j)->state = SHOWN;
        }
    }
}