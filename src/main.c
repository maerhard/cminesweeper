#define SDL_MAIN_HANDLED

#include <stdbool.h>
#include <stdio.h>
#include <time.h>

#include "minesweeper.h"
#include "viewport.h"

#define LINES 10
#define COLUMNS 20
#define MINES 40
#define WIDTH 800
#define HEIGHT 600

int main() {
    //srand(0);
    srand(time(NULL));

    Minesweeper* minesweeper = new_minesweeper(LINES, COLUMNS, MINES);

    if(minesweeper) {
        Viewport* viewport = new_viewport(WIDTH, HEIGHT, minesweeper);
            
        if (viewport) {
            event_loop(viewport);
            close_viewport(viewport);
        }
    }

    free_minesweeper(minesweeper);
    return 0;
}