#ifndef __DRAW_H__
#define __DRAW_H__


#include <stdbool.h>

#include "viewport.h"

/**
 * @brief Draws a cell.
 * 
 * @param viewport the viewport.
 * @param cell the cell to draw.
 * @param x the x pos of the cell on the window.
 * @param y the y pos of the cell on the window.
 * @param hover true if the mouse is on the cell.
 */
void draw_cell(Viewport* viewport, Cell* cell, int x, int y, bool hover);

/**
 * @brief Draws the minesweeper on a limited area of the window.
 * 
 * @param viewport the viewport to draw.
 */
void draw_minesweeper(Viewport* viewport);

/**
 * @brief Draws the minesweeper of a viewport in the terminal.
 * 
 * @param viewport the viewport to draw.
 */
void draw_minesweeper_txt(Viewport* viewport);

/**
 * @brief Draws the viewport.
 * 
 * @param viewport the viewport to draw.
 */
void draw_viewport(Viewport* viewport);

#endif