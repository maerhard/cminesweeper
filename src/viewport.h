#ifndef __VIEWPORT_H__
#define __VIEWPORT_H__


#include <SDL2/SDL.h>

#include "minesweeper.h"

typedef enum appState {
    MENU = 0,
    GAME,
    QUIT
} AppState;

typedef struct {
    int x_min;
    int x_max;
    int y_min;
    int y_max;
    int size_cell;
    int border_x;
    int border_y;
    int x_mouse;
    int y_mouse;
    int i_mouse;
    int j_mouse;
} VMS; //Viewport MineSweeper

typedef struct {
    int width;
    int height;
    SDL_Window* window;
    SDL_Renderer* renderer;
    SDL_Texture* texture;
    VMS vms;
    Minesweeper* minesweeper;
    AppState state;
} Viewport;


/**
 * @brief Initializes all the SDL subsystems and creates a new viewport with an already-made minesweeper.
 * 
 * @param width the width of the window.
 * @param height the height of the window.
 * @param minesweeper the minesweeper.
 * @return Viewport* 
 */
Viewport* new_viewport(int width, int height, Minesweeper* minesweeper);

/**
 * @brief Updates the drawing area of the minesweeper.
 * 
 * @param viewport the viewport to update.
 * @param ms if it updates the minesweeper parameters.
 * @param mouse if it updates the mouse parameters
 */
void update_vms(Viewport* viewport, bool ms, bool mouse);

/**
 * @brief Loops to update the app when in a game.
 * 
 * @param viewport the viewport to update each loop.
 */
void event_loop_game(Viewport* viewport);

/**
 * @brief Loops to update the app.
 * 
 * @param viewport the viewport to update each loop.
 */
void event_loop(Viewport* viewport);

/**
 * @brief Closes the viewport and all the SDL subsystems.
 * 
 * @param viewport the viewport to close
 */
void close_viewport(Viewport* viewport);


#endif