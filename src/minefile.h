#ifndef __MINELINKEDARRAY_H__
#define __MINELINKEDARRAY_H__

#define MAX_FILE 3000

typedef struct {
    int i;
    int j;
} Position;

typedef struct {
    int reading_index;
    int writing_index;
    Position file[MAX_FILE];
} File;

/**
 * @brief Creates a new file
 * 
 * @return File* 
 */
File* new_file();

/**
 * @brief Frees a file
 * 
 */
void free_file(File*);

/**
 * @brief Checks if a file is empty
 * 
 * @return 1 if it is empty, 0 if it is not
 */
int is_empty_file(File*);

/**
 * @brief Inserts a position in a file
 * 
 */
void insert_in_file(File*, Position);

/**
 * @brief Takes out the first position in a file
 * 
 * @return the position. Do not use this position if the file was empty
 */
Position remove_from_file(File*);

/**
 * @brief Empties a file if needed
 * 
 */
void empty_file(File*);

#endif