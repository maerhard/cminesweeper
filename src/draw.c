#include <SDL2/SDL.h>
#include <stdio.h>

#include "minesweeper.h"
#include "maths.h"
#include "draw.h"

void draw_cell(Viewport* viewport, Cell* cell, int x, int y, bool hover) {
    SDL_Rect rect_cell, src, dest;
    rect_cell.x = x;
    rect_cell.y = y;
    rect_cell.w = rect_cell.h = viewport->vms.size_cell;

    src.x = src.y = 0;
    src.w = src.h = 9;

    dest.x = x;
    dest.y = y;
    dest.w = dest.h = viewport->vms.size_cell;

    if (cell->state == SHOWN) {
        SDL_SetRenderDrawColor(viewport->renderer, 50, 50, 50, 255);
        SDL_RenderFillRect(viewport->renderer, &rect_cell);
        rect_cell.x += 0.02*viewport->vms.size_cell;
        rect_cell.y += 0.02*viewport->vms.size_cell;
        rect_cell.w = rect_cell.h = 0.96*viewport->vms.size_cell;
        SDL_SetRenderDrawColor(viewport->renderer, 190, 190, 190, 255);
        SDL_RenderFillRect(viewport->renderer, &rect_cell);

        if (cell->neighbour_mines > 0 && cell->mine == 0) {
            src.y = (cell->neighbour_mines - 1)*src.h;
            SDL_RenderCopy(viewport->renderer, viewport->texture, &src, &dest);
        }
        if (cell->mine == 1) {
            src.y = 8*src.h;
            SDL_RenderCopy(viewport->renderer, viewport->texture, &src, &dest);
        }
    }
    else {
        if (hover) {
            SDL_SetRenderDrawColor(viewport->renderer, 220, 220, 220, 255);
            SDL_RenderFillRect(viewport->renderer, &rect_cell);
            rect_cell.x += 0.05*viewport->vms.size_cell;
            rect_cell.y += 0.05*viewport->vms.size_cell;
            rect_cell.w = rect_cell.h = 0.95*viewport->vms.size_cell;
            SDL_SetRenderDrawColor(viewport->renderer, 120, 120, 120, 255);
            SDL_RenderFillRect(viewport->renderer, &rect_cell);
            rect_cell.w = rect_cell.h = 0.90*viewport->vms.size_cell;
            SDL_SetRenderDrawColor(viewport->renderer, 170, 170, 170, 255);
            SDL_RenderFillRect(viewport->renderer, &rect_cell);
        }
        else {
            SDL_SetRenderDrawColor(viewport->renderer, 200, 200, 200, 255);
            SDL_RenderFillRect(viewport->renderer, &rect_cell);
            rect_cell.x += 0.05*viewport->vms.size_cell;
            rect_cell.y += 0.05*viewport->vms.size_cell;
            rect_cell.w = rect_cell.h = 0.95*viewport->vms.size_cell;
            SDL_SetRenderDrawColor(viewport->renderer, 100, 100, 100, 255);
            SDL_RenderFillRect(viewport->renderer, &rect_cell);
            rect_cell.w = rect_cell.h = 0.90*viewport->vms.size_cell;
            SDL_SetRenderDrawColor(viewport->renderer, 150, 150, 150, 255);
            SDL_RenderFillRect(viewport->renderer, &rect_cell);
        }

        switch (cell->state) {
            case FLAGGED:
                src.y = 9*src.h;
                SDL_RenderCopy(viewport->renderer, viewport->texture, &src, &dest);
                break;
            case QUESTION_MARK:
                src.y = 10*src.h;
                SDL_RenderCopy(viewport->renderer, viewport->texture, &src, &dest);
                break;
        }
    }
}

void draw_minesweeper(Viewport* viewport) {
    SDL_SetRenderDrawColor(viewport->renderer, 100, 100, 100, 255);
    SDL_Rect minesweeper_bg;
    minesweeper_bg.x = viewport->vms.x_min;
    minesweeper_bg.y = viewport->vms.y_min;
    minesweeper_bg.w = viewport->vms.x_max - viewport->vms.x_min;
    minesweeper_bg.h = viewport->vms.y_max - viewport->vms.y_min;
    SDL_RenderFillRect(viewport->renderer, &minesweeper_bg);

    for (int i=0; i<viewport->minesweeper->lines; i++) {
        for (int j=0; j<viewport->minesweeper->columns; j++) {
            draw_cell(viewport,
                viewport->minesweeper->minesweeper + i*viewport->minesweeper->columns + j,
                viewport->vms.x_min + viewport->vms.border_x + j*viewport->vms.size_cell,
                viewport->vms.y_min + viewport->vms.border_y + i*viewport->vms.size_cell,
                (i==viewport->vms.i_mouse && j==viewport->vms.j_mouse)
            );
        }
    }
}

void draw_minesweeper_txt(Viewport* viewport) {
    Cell* cell;
    Minesweeper* ms = viewport->minesweeper;
    for (int i=0; i<ms->lines; i++) {
        for (int j=0; j<ms->columns; j++) {
            cell=cell_from_coord(ms, i, j);
            switch (cell->state) {
                case HIDDEN:
                    printf("#");
                break;

                case SHOWN:
                    if (cell->mine) {
                        printf("¤");
                    }
                    else {
                        printf("%d", cell->neighbour_mines);
                    }
                break;

                case FLAGGED:
                    printf("d");
                break;

                case QUESTION_MARK:
                    printf("?");
                break;
            }
        }
        printf("\n");
    }
    printf("\n\n");
}

void draw_viewport(Viewport* viewport) {
    //draw_bg();
    draw_minesweeper(viewport);
}