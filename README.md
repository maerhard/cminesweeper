# CMinesweeper

A personal minesweeper made in C with SDL.
Made for fun and to improve my programming skills, don't expect a lot from it.


# TODO List (somewhat in order):

-Make the game actually end when you win/lose |DONE|
-Make a background, which will also show the time spent and (mines - flags)
-End screens |DONE|
-Show all mines when defeat |DONE| (maybe show in red the mine that exploded) 
-Save/load functionnalities
-Difficulty choice
-Custom difficulties
-Keyboard controls
-Camera/zoom (this will be fantastic to make this working with keyboard controls :D)