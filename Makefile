all: bin/minesweeper.o bin/minefile.o bin/viewport.o bin/draw.o bin/maths.o
	gcc src/main.c bin/minesweeper.o bin/minefile.o bin/viewport.o bin/draw.o bin/maths.o -o game -Wall -Wextra -g -lSDL2 -lSDL2main -lSDL2_image

bin/minesweeper.o : src/minesweeper.c
	gcc -c src/minesweeper.c -o bin/minesweeper.o -g

bin/minefile.o : src/minefile.c
	gcc -c src/minefile.c -o bin/minefile.o -g

bin/viewport.o : src/viewport.c
	gcc -c src/viewport.c -o bin/viewport.o -g

bin/draw.o : src/draw.c
	gcc -c src/draw.c -o bin/draw.o -g

bin/maths.o : src/maths.c
	gcc -c src/maths.c -o bin/maths.o -g

clean:
	del bin\mine.o bin\minesweeper.o bin\viewport.o bin\draw.o bin\maths.o game